<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Pilih Bagian</h4>
</div>
{!! Form::open(['role' => 'form', 'id' => 'bagian-outbox']) !!}
{!! Form::hidden('id', $outbox->id) !!}
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">

            <div class="form-body">
                <div class="form-group form-md-checkboxes">
                    <label>Bagian Yang Akan Dikirimkan Dokumen</label>
                    <div class="md-checkbox-list">
                        @foreach($bagian as $row)
                            <div class="md-checkbox">
                                <input type="checkbox" name="{!! $row->id !!}" id="{!! $row->id !!}" class="md-check">
                                <label for="{!! $row->id !!}">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span>
                                    {!! $row->nama !!} </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">TUTUP</button>
    <button type="submit" class="btn blue">SIMPAN</button>
</div>
{!! Form::close() !!}

<script>
    (function() {
        var Form = function() {
            this.formId = 'form#bagian-outbox';
        };
        Form.prototype.store = function() {
            var self = this;
            $(self.formId).on('submit', function(e) {
                e.preventDefault();

                $.ajax({
                    method: "POST",
                    url: '{!! route("outbox.bagian-store") !!}',
                    data: $(self.formId).serialize()
                }).done(function(html) {
                    console.log(html);
                });
            })
        }

        var f = new Form();

        f.store();
    })();
</script>
