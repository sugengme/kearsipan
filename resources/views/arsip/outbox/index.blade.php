<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green">
                    <i class="icon-pin font-green"></i>
                    <span class="caption-subject bold uppercase"> Filter Dokumen</span>
                </div>
            </div>
            <div class="portlet-body form">
                {!! Form::open(['role' => 'form', 'id' => 'filter-outbox']) !!}
                    <div class="form-body">
                        <div class="row" style="margin-bottom: 25px;">
                            <div class="col-md-3">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    {!! Form::text('nomor_surat', '', ['class'=>'form-control']) !!}
                                    {!! Form::label('nomor_surat', 'Nomor Dokumen') !!}
                                    <span class="help-block">Nomor dokumen yang akan dicari.</span>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 25px;">
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    {!! Form::text('dari_tanggal', '', ['class'=>'form-control date-picker', 'style'=>'text-align:center;']) !!}
                                    {!! Form::label('dari_tanggal', 'Dari Tanggal') !!}
                                    <span class="help-block">Tanggal awal dokumen yang akan dicari.</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    {!! Form::text('sampai_tanggal', '', ['class'=>'form-control date-picker', 'style'=>'text-align:center;']) !!}
                                    {!! Form::label('sampai_tanggal', 'Sampai Tanggal') !!}
                                    <span class="help-block">Tanggal Akhir dokumen yang akan dicari.</span>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 25px;">
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    {!! Form::select('jenis_id', $form->options('JenisSurat'), '', ['class' => 'form-control']) !!}
                                    {!! Form::label('jenis_id', 'Jenis Dokumen') !!}
                                    <span class="help-block">Jenis dokumen yang akan dicari.</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    {!! Form::text('perihal', '', ['class'=>'form-control']) !!}
                                    {!! Form::label('perihal', 'Perihal') !!}
                                    <span class="help-block">Perihal dokumen yang akan dicari.</span>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 25px;">
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    {!! Form::select('kepentingan_id', $form->options('Kepentingan'), '', ['class' => 'form-control']) !!}
                                    {!! Form::label('kepentingan_id', 'Kepentingan') !!}
                                    <span class="help-block">Kepentingan dokumen yang akan dicari.</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    {!! Form::select('kerahasiaan_id', $form->options('Kerahasiaan'), '', ['class' => 'form-control']) !!}
                                    {!! Form::label('kerahasiaan_id', 'Kerahasiaan') !!}
                                    <span class="help-block">Kerahasiaan dokumen yang akan dicari.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions noborder">
                        <button type="submit" class="btn blue">Submit</button>
                        <button type="button" class="btn default">Batal</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<div class="row" id="content-grid"></div>

<script>
    (function() {
        var Form = function() {
            this.formId = 'form#filter-outbox';
        };
        Form.prototype.fetch = function() {
            var self = this;
            $(self.formId).on('submit', function(e) {
                e.preventDefault();

                $.ajax({
                    method: "POST",
                    url: '{!! route("outbox.grid") !!}',
                    data: $(self.formId).serialize()
                }).done(function(html) {
                    $('#content-grid').html(html);
                });
            })
        }

        var f = new Form();

        f.fetch();

        $('.date-picker').datepicker({
            format: 'dd MM yyyy',
            orientation: "left",
            autoclose: true
        });
    })();
</script>