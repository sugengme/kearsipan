<?php
    $n = 1;
?>

<div class="col-md-12">
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>DAFTAR DOKUMEN
            </div>
        </div>
        <div class="portlet-body flip-scroll" id="table-surat">
            <div class="btn-group btn-group-solid margin-bottom-10">
                <button type="button" class="btn blue" id="add-outbox">TAMBAH DOKUMEN</button>
            </div>
            @if(sizeof($outbox) > 0)
            <table class="table table-bordered table-striped table-condensed flip-content center-head">
                <thead class="flip-content">
                    <tr>
                        <th>#</th>
                        <th>No. Surat</th>
                        <th>Tgl. Surat</th>
                        <th>Perihal</th>
                        <th>Jenis</th>
                        <th>Kep</th>
                        <th>Rhs</th>
                        <th>Lkp</th>
                        <th>Doc</th>
                        <th colspan="2">Action</th>
                        <th>Bagian</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($outbox as $row)
                    <tr>
                        <td>{!! str_pad($n++, 3, '0', STR_PAD_LEFT); !!}</td>
                        <td>{!! $row->nomor_surat !!}</td>
                        <td>{!! $row->tanggal_surat->format('d F Y') !!}</td>
                        <td>{!! $row->perihal !!}</td>
                        <td class="text-center">
                            <button type="button" class="btn red btn-xs" title="{!! $row->jenis->nama !!}">{!! $row->jenis->kode !!}</button>
                        </td>
                        <td class="text-center">
                            <button type="button" class="btn purple btn-xs" title="{!! $row->penting->nama !!}">{!! $row->penting->kode !!}</button>
                        </td>
                        <td class="text-center">
                            <button type="button" class="btn yellow btn-xs" title="{!! $row->rahasia->nama !!}">{!! $row->rahasia->kode !!}</button>
                        </td>
                        <td class="text-center">
                            <button type="button" class="btn dark btn-xs" title="{!! $row->lingkup->nama !!}">{!! $row->lingkup->kode !!}</button>
                        </td>
                        <td class="text-center"><a href="javascript:;" class="btn btn-icon-only dark" data-upload="true" title="UPLOAD DOKUMEN"><i class="fa fa-upload"></i></a></td>
                        <td class="text-center"><a href="javascript:;" class="btn btn-icon-only green" data-add="true"><i class="fa fa-edit"></i></a></td>
                        <td class="text-center"><a href="javascript:;" class="btn btn-icon-only red"><i class="fa fa-times"></i></a></td>
                        <td class="text-center"><a href="javascript:;" class="btn btn-icon-only blue" data-url="{!! route('outbox.bagian', $row->id) !!}" data-bagian="true"><i class="fa fa-plus" title="Tambah Bagian yang akan dikirim"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
                <div class="row">
                    <div class="col-md-12 text-center">
                        {!! $outbox->render() !!}
                    </div>
                </div>
            @else
                <div class="alert alert-warning">
                    <strong>Peringatan!</strong> Tidak ditemukan data surat outbox
                </div>
            @endif
        </div>
    </div>
</div>

<div id="ajax-modal" class="modal container fade" tabindex="-1" data-backdrop="static" data-keyboard="false"></div>

<script>
    $('.pagination a').on('click', function (event) {
        event.preventDefault();
        if ( $(this).attr('href') != '#' ) {
            $("html, body").animate({ scrollTop: 0 }, "fast");
            $('#table-surat').load($(this).attr('href'));
        }
    });

    $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
            '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar" style="width: 100%;"></div>' +
            '</div>' +
            '</div>';

    $.fn.modalmanager.defaults.resize = true;

    var $modal = $('#ajax-modal');

    $('#add-outbox').on('click', function(){
        $('body').modalmanager('loading');
        setTimeout(function(){
            $modal.load('{!! route("outbox.create") !!}', '', function(){
                $modal.modal();
            });
        }, 500);
    });

    $('[data-upload="true"]').on('click', function() {
        $('body').modalmanager('loading');
        setTimeout(function(){
            $modal.load('{!! route("outbox.upload") !!}', '', function(){
                $modal.modal();
            });
        }, 500);
    });

    $('[data-bagian="true"]').on('click', function() {
        var self = this;
        $('body').modalmanager('loading');
        setTimeout(function(){
            var url = $(self).data('url');
            $modal.load(url, '', function(){
                $modal.modal();
            });
        }, 500);
    });
</script>