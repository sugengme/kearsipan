<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Tambah Dokumen</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['role' => 'form', 'id' => 'filter-outbox']) !!}
            <div class="form-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-3">
                        <div class="form-group form-md-line-input form-md-floating-label">
                            {!! Form::text('nomor_surat', '', ['class'=>'form-control']) !!}
                            {!! Form::label('nomor_surat', 'Nomor Surat') !!}
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-3">
                        <div class="form-group form-md-line-input form-md-floating-label">
                            {!! Form::text('tanggal_surat', '', ['class'=>'form-control date-picker', 'style'=>'text-align:center;']) !!}
                            {!! Form::label('tanggal_surat', 'Tanggal Surat') !!}
                        </div>
                    </div>
                </div>
                <div class="row"style="margin-bottom: 10px;">
                    <div class="col-md-10">
                        <div class="form-group form-md-line-input form-md-floating-label">
                            {!! Form::text('perihal', '', ['class'=>'form-control']) !!}
                            {!! Form::label('perihal', 'Perihal') !!}
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-4">
                        <div class="form-group form-md-line-input form-md-floating-label">
                            {!! Form::select('jenis_id', $form->options('JenisSurat'), '', ['class' => 'form-control']) !!}
                            {!! Form::label('jenis_id', 'Jenis Surat') !!}
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-2">
                        <div class="form-group form-md-line-input form-md-floating-label">
                            {!! Form::select('lingkup_id', $form->options('RuangLingkup'), '', ['class' => 'form-control']) !!}
                            {!! Form::label('lingkup_id', 'Ruang Lingkup') !!}
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group form-md-line-input form-md-floating-label">
                            {!! Form::select('kepentingan_id', $form->options('Kepentingan'), '', ['class' => 'form-control']) !!}
                            {!! Form::label('kepentingan_id', 'Kepentingan') !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-md-line-input form-md-floating-label">
                            {!! Form::select('kerahasiaan_id', $form->options('Kerahasiaan'), '', ['class' => 'form-control']) !!}
                            {!! Form::label('kerahasiaan_id', 'Kerahasiaan') !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">TUTUP</button>
    <button type="button" class="btn blue">SIMPAN</button>
</div>

<script>
    $('.date-picker').datepicker({
        format: 'dd MM yyyy',
        orientation: "left",
        autoclose: true
    });
</script>