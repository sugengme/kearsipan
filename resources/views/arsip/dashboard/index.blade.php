<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light blue-soft" href="javascript:;">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    INBOX
                </div>
                <div class="desc">
                    Inbox (199)
                </div>
                <div class="desc">
                    Un-Read (199)
                </div>
                <div class="desc">
                    Today (199)
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light red-soft" href="javascript:;">
            <div class="visual">
                <i class="fa fa-trophy"></i>
            </div>
            <div class="details">
                <div class="number">
                    OUTBOX
                </div>
                <div class="desc">
                    Inbox (0)
                </div>
                <div class="desc">
                    Un-Read (0)
                </div>
                <div class="desc">
                    Today (0)
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light green-soft" href="javascript:;">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    DISPOSISI
                </div>
                <div class="desc">
                    Inbox (0)
                </div>
                <div class="desc">
                    Un-Read (0)
                </div>
                <div class="desc">
                    Today (0)
                </div>
            </div>
        </a>
    </div>
</div>
<!-- END DASHBOARD STATS -->
<div class="clearfix">