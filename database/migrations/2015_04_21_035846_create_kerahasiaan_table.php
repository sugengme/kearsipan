<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKerahasiaanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kerahasiaan', function(Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 1);
            $table->string('nama', 100);
            $table->enum('aktif', ['1', '0'])->default('1');;
            $table->timestamps();
        });

        $this->fill();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kerahasiaan');
	}

	/**
	 * Fill with default values
	 *
	 * @return void
	 */
	private function fill()
	{
		DB::table('kerahasiaan')->insert(
			[
				[
					'kode' => 'A',
					'nama' => 'Sangat Rahasia'
				],
				[
					'kode' => 'B',
					'nama' => 'Rahasia'
				],
				[
					'kode' => 'C',
					'nama' => 'Biasa'
				]
			]
		);
	}
}
