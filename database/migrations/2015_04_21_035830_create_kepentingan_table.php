<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKepentinganTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kepentingan', function(Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 1);
            $table->string('nama', 100);
			$table->string('color', 20);
            $table->enum('aktif', ['1', '0'])->default('1');;
            $table->timestamps();
        });

        $this->fill();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kepentingan');
	}

	/**
	 * Fill with default values
	 *
	 * @return void
	 */
	private function fill()
	{
		DB::table('kepentingan')->insert(
			[
				[
					'kode' => 'A',
					'nama' => 'Sangat Penting',
					'color' => 'red'
				],
				[
					'kode' => 'B',
					'nama' => 'Penting',
					'color'=> 'yellow'
				],
				[
					'kode' => 'C',
					'nama' => 'Biasa',
					'color' => 'green'
				]
			]
		);
	}
}
