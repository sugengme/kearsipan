<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBagianTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bagian', function(Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 10);
            $table->string('nama', 50);
            $table->string('created_user', 10);
            $table->string('updated_user', 10);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bagian');
	}

}
