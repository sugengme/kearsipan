<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('inbox_id');
            $table->tinyInteger('to_bagian_id');
            $table->tinyInteger('from_bagian_id');
            $table->text('content');
            $table->datetime('tanggal_kirim');
            $table->integer('user_id');
            $table->string('created_user', 15);
            $table->string('updated_user', 15);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}

}
