<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBagianOutboxPivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bagian_outbox', function(Blueprint $table)
		{
			$table->integer('bagian_id')->unsigned()->index();
			$table->foreign('bagian_id')->references('id')->on('bagian')->onDelete('cascade');
			$table->integer('outbox_id')->unsigned()->index();
			$table->foreign('outbox_id')->references('id')->on('outbox')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bagian_outbox');
	}

}
