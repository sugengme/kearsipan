<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutboxTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('outbox', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nomor_surat');
            $table->date('tanggal_surat');
            $table->string('perihal', 255);
            $table->tinyInteger('jenis_id');
            $table->tinyInteger('kepentingan_id');
            $table->tinyInteger('kerahasiaan_id');
            $table->tinyInteger('lingkup_id');
            $table->tinyInteger('from_bagian_id');
            $table->tinyInteger('to_bagian_id');
            $table->date('tanggal_kirim');
            $table->integer('user_kirim');
            $table->enum('status', ['1', '0'])->default('0');
			$table->string('created_user', 50);
			$table->string('updated_user', 50);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('outbox');
	}

}
