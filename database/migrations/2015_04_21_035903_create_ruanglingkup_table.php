<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRuanglingkupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ruanglingkup', function(Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 1);
            $table->string('nama', 100);
            $table->enum('aktif', ['1', '0'])->default('1');;
            $table->timestamps();
        });

        $this->fill();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ruanglingkup');
	}

	/**
	 * Fill with default values
	 *
	 * @return void
	 */
	private function fill()
	{
		DB::table('ruanglingkup')->insert(
			[
				[
					'kode' => 'A',
					'nama' => 'Bidang Akademik'
				],
				[
					'kode' => 'B',
					'nama' => 'Bidang Umum'
				],
				[
					'kode' => 'C',
					'nama' => 'Bidang Keuangan'
				],
				[
					'kode' => 'D',
					'nama' => 'Bidang Kemahasiswaan'
				],
				[
					'kode' => 'E',
					'nama' => 'Bidang Lain-lain'
				]
			]
		);
	}

}
