<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisSuratTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jenis_surat', function(Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 1);
            $table->string('nama', 100);
			$table->enum('aktif', ['1', '0'])->default('1');
            $table->timestamps();
        });

        $this->fill();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jenis_surat');
	}

    /**
     * Fill with default values
     *
     * @return void
     */
    private function fill()
    {
        DB::table('jenis_surat')->insert(
            [
                [
                    'kode' => 'A',
                    'nama' => 'Surat Keputusan'
                ],
                [
                    'kode' => 'B',
                    'nama' => 'Surat Keterangan'
                ],
                [
                    'kode' => 'C',
                    'nama' => 'Surat Tugas/Memo'
                ],
                [
                    'kode' => 'D',
                    'nama' => 'Surat Pemberitahuan'
                ],
                [
                    'kode' => 'E',
                    'nama' => 'Surat Undangan'
                ]
            ]
        );
    }

}
