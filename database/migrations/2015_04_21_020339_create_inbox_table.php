<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInboxTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inbox', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('outbox_id');
            $table->enum('status', ['1', '0'])->default('0');
			$table->string('created_user', 15);
			$table->string('updated_user', 15);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inbox');
	}

}
