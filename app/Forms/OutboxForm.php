<?php  namespace App\Forms;

use App;

class OutboxForm {
    public function options($model) {
        return App::make("App\\{$model}")->options();
    }
}
