<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruanglingkup extends Model {

	protected $table = 'ruanglingkup';

	public function options()
	{
		return ['' => ''] + $this->where('aktif', '1')->lists('nama', 'id');
	}
}
