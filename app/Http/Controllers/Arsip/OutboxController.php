<?php  namespace App\Http\Controllers\Arsip; 

use App\Bagian;
use App\Forms\OutboxForm;
use App\Http\Controllers\Controller;
use App\Outbox;
use Illuminate\Http\Request;

class OutboxController extends Controller {
    public function index(OutboxForm $form)
    {
        return view('arsip.outbox.index')->with('form', $form);
    }

    public function grid(Outbox $outbox, Request $request)
    {
        $filter = $request->all();

        $result = $outbox->search($filter)->paginate(15);

        return view('arsip.outbox._grid')->with('outbox', $result);
    }

    public function show($id)
    {
        return "YOOO";
    }

    public function create(OutboxForm $form)
    {
        return view('arsip.outbox._form_add')->with('form', $form);
    }

    public function upload()
    {
        return view('arsip.outbox._form_upload');
    }

    public function bagian($id)
    {
        $bagian = Bagian::take(10)->get(['id', 'kode', 'nama']);

        return view('arsip.outbox._form_bagian')->with('bagian', $bagian);
    }

    public function bagianCreate(Request $request)
    {
        $bagian = $request->all();

        dd($bagian);
    }
}