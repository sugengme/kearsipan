<?php  namespace App\Http\Controllers\Arsip;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {
    public function index()
    {
        return view('arsip.dashboard.index');
    }
}