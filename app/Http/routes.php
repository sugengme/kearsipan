<?php

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['namespace' => 'Arsip'], function () {
    Route::resource('module', 'ModuleController', ['only' => ['index']]);
    Route::resource('dashboard', 'DashboardController', ['only' => ['index']]);

    Route::post('outbox/grid', [
        'as' => 'outbox.grid',
        'uses' => 'OutboxController@grid'
    ]);

    Route::get('outbox/upload', [
        'as' => 'outbox.upload',
        'uses' => 'OutboxController@upload'
    ]);

    Route::get('outbox/bagian/{id}', [
        'as' => 'outbox.bagian',
        'uses' => 'OutboxController@bagian'
    ])->where('id', '[0-9]+');

    Route::post('outbox/bagian', [
        'as' => 'outbox.bagian-store',
        'uses' => 'OutboxController@bagianCreate'
    ]);

    Route::resource('outbox', 'OutboxController');
});
