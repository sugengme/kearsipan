<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisSurat extends Model {

	protected $table = 'jenis_surat';

	public function options()
	{
	    return ['' => ''] + $this->where('aktif', '1')->lists('nama', 'id');
	}
}
