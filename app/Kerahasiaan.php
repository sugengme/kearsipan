<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Kerahasiaan extends Model {

    protected $table = 'kerahasiaan';

    public function options()
    {
        return ['' => ''] + $this->where('aktif', '1')->lists('nama', 'id');
    }

}
