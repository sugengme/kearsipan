<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Outbox extends Model {

    protected $table = 'outbox';

    public function getDates()
    {
        return ['tanggal_surat'];
    }

    public function scopeSearch($q, $filter)
    {
        return $q->where(function($query) use ($filter) {
            if ($filter['nomor_surat']) $query->where('nomor_surat', 'LIKE', '%'.$filter['nomor_surat'].'%');
            if ($filter['dari_tanggal']) {
                if ($filter['sampai_tanggal']) {
                    $query->whereBetween('tanggal_surat', [$this->castDate($filter['dari_tanggal']), $this->castDate($filter['sampai_tanggal'])]);
                } else {
                    $query->where('tanggal_surat', $this->castDate($filter['dari_tanggal']));
                }
            }
            if ($filter['jenis_id']) $query->where('jenis_id', $filter['jenis_id']);
            if ($filter['kepentingan_id']) $query->where('kepentingan_id', $filter['kepentingan_id']);
            if ($filter['kerahasiaan_id']) $query->where('kerahasiaan_id', $filter['kerahasiaan_id']);
            if ($filter['perihal']) $query->where('perihal', "LIKE", '%'.$filter['perihal'].'%');
        });
    }

    public function inbox()
    {
        return $this->hasOne('App\Inbox');
    }

    public function jenis()
    {
        return $this->belongsTo('App\JenisSurat', 'jenis_id');
    }

    public function rahasia()
    {
        return $this->belongsTo('App\Kerahasiaan', 'kerahasiaan_id');
    }

    public function penting()
    {
        return $this->belongsTo('App\Kepentingan', 'kepentingan_id');
    }

    public function lingkup()
    {
        return $this->belongsTo('App\RuangLingkup', 'lingkup_id');
    }

    public function toBagian()
    {
        return $this->belongsTo('App\Bagian', 'to_bagian_id');
    }

    public function fromBagian()
    {
        return $this->belongsTo('App\Bagian', 'from_bagian_id');
    }

    private function castDate($date) {
        return Carbon::createFromFormat('d F Y', $date)->toDateString();
    }
}
