<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Kepentingan extends Model {

	protected $table = 'kepentingan';

	public function options()
	{
		return ['' => ''] + $this->where('aktif', '1')->lists('nama', 'id');
	}

}
