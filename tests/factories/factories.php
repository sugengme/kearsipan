<?php

$factory('App\Bagian', [
    'kode' => $faker->countryCode,
    'nama' => $faker->name,
    'created_user' => 'Sugeng'
]);

$factory('App\JenisSurat', [
    'kode' => $faker->randomLetter,
    'nama' => $faker->streetName
]);

$factory('App\Kepentingan', [
    'kode' => $faker->randomLetter,
    'nama' => $faker->name,
]);

$factory('App\Kerahasiaan', [
    'kode' => $faker->randomLetter,
    'nama' => $faker->colorName,
]);

$factory('App\RuangLingkup', [
    'kode' => $faker->randomLetter,
    'nama' => $faker->domainName,
]);

$factory('App\Outbox', function($faker) {

    $jenis = \App\JenisSurat::lists('id');
    $kepentingan = \App\Kepentingan::lists('id');
    $kerahasiaan = \App\Kerahasiaan::lists('id');
    $lingkup     = \App\Ruanglingkup::lists('id');

    return [
        'nomor_surat'    => $faker->numberBetween(100000, 200000),
        'tanggal_surat'  => $faker->date(),
        'perihal'        => $faker->sentence(),
        'jenis_id'       => $faker->randomElement($jenis),
        'kepentingan_id' => $faker->randomElement($kepentingan),
        'kerahasiaan_id' => $faker->randomElement($kerahasiaan),
        'lingkup_id'     => $faker->randomElement($lingkup),
        'from_bagian_id' => 'factory:App\Bagian',
        'to_bagian_id'   => 'factory:App\Bagian',
        'tanggal_kirim'  => $faker->date()
    ];
});